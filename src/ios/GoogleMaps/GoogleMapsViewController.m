//
//  GoogleMapsViewController.m
//  cordova-googlemaps-plugin v2
//
//  Created by Masashi Katsumata.
//
//

#import "GoogleMapsViewController.h"
#if CORDOVA_VERSION_MIN_REQUIRED < __CORDOVA_4_0_0
#import <Cordova/CDVJSON.h>
#endif


@implementation GoogleMapsViewController

- (id)initWithOptions:(NSDictionary *) options {
  self = [super init];
  self.plugins = [NSMutableDictionary dictionary];
  self.isFullScreen = NO;
  self.screenSize = [[UIScreen mainScreen] bounds];
  self.screenScale = [[UIScreen mainScreen] scale];
  self.clickable = YES;
  self.isRenderedAtOnce = NO;
  self.mapDivId = nil;
  self.objects = [[NSMutableDictionary alloc] init];
  self.executeQueue =  [NSOperationQueue new];
  self.executeQueue.maxConcurrentOperationCount = 10;


  return self;
}

- (void)viewDidLoad
{
  [super viewDidLoad];

}


- (void)didReceiveMemoryWarning
{
  [super didReceiveMemoryWarning];
}

/**
 * Called when the My Location button is tapped.
 *
 * @return YES if the listener has consumed the event (i.e., the default behavior should not occur),
 *         NO otherwise (i.e., the default behavior should occur). The default behavior is for the
 *         camera to move such that it is centered on the user location.
 */
- (BOOL)didTapMyLocationButtonForMapView:(GMSMapView *)mapView {

  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: '%@', callback: '_onMapEvent', args: []});",
                        self.mapId, @"my_location_button_click"];
  [self execJS:jsString];
  return NO;
}

#pragma mark - GMSMapViewDelegate

/**
 * @callback the my location button is clicked.
 */
- (void)mapView:(GMSMapView *)mapView didTapAtCoordinate:(CLLocationCoordinate2D)coordinate {

  if (self.activeMarker) {
    /*
     NSString *clusterId_markerId =[NSString stringWithFormat:@"%@", self.activeMarker.userData];
     NSArray *tmp = [clusterId_markerId componentsSeparatedByString:@"_"];
     NSString *className = [tmp objectAtIndex:0];
     if ([className isEqualToString:@"markercluster"]) {
     if ([clusterId_markerId containsString:@"-marker_"]) {
     [self triggerClusterEvent:@"info_close" marker:self.activeMarker];
     }
     } else {
     [self triggerMarkerEvent:@"info_close" marker:self.activeMarker];
     }
     */

    //self.map.selectedMarker = nil;
    self.activeMarker = nil;
  }

  //NSArray *pluginNames =[self.plugins allKeys];
  //NSString *pluginName;
  NSString *key;
  NSDictionary *properties;
  //CDVPlugin<MyPlgunProtocol> *plugin;
  GMSCoordinateBounds *bounds;
  GMSPath *path;
  NSArray *keys;
  NSNumber *isVisible, *geodesic, *isClickable;
  NSMutableArray *boundsHitList = [NSMutableArray array];
  //NSMutableArray *boundsPluginList = [NSMutableArray array];
  int i,j;
  float zIndex, maxZIndex;
  NSString* hitKey = nil;


  keys = [self.objects allKeys];
  for (j = 0; j < [keys count]; j++) {
    key = [keys objectAtIndex:j];
    if ([key containsString:@"-marker"] ||
        [key containsString:@"property"] == NO) {
      continue;
    }

    properties = [self.objects objectForKey:key];
    //NSLog(@"--> key = %@, properties = %@", key, properties);

    // Skip invisible polyline
    isVisible = (NSNumber *)[properties objectForKey:@"isVisible"];
    if ([isVisible boolValue] == NO) {
      //NSLog(@"--> key = %@, isVisible = NO", key);
      continue;
    }

    // Skip isClickable polyline
    isClickable = (NSNumber *)[properties objectForKey:@"isClickable"];
    if ([isClickable boolValue] == NO) {
      //NSLog(@"--> key = %@, isClickable = NO", key);
      continue;
    }
    //NSLog(@"--> key = %@, isVisible = YES, isClickable = YES", key);

    // Skip if the click point is out of the polyline bounds.
    bounds = (GMSCoordinateBounds *)[properties objectForKey:@"bounds"];
    if ([bounds containsCoordinate:coordinate]) {
      [boundsHitList addObject:key];
      //[boundsPluginList addObject:plugin];
    }

  }
  /*
   for (i = 0; i < [pluginNames count]; i++) {
   pluginName = [pluginNames objectAtIndex:i];

   // Skip marker class
   if ([pluginName hasSuffix:@"-marker"]) {
   continue;
   }

   // Get the plugin (marker, polyline, polygon, circle, groundOverlay)
   plugin = [self.plugins objectForKey:pluginName];


   keys = [plugin.objects allKeys];
   for (j = 0; j < [keys count]; j++) {
   key = [keys objectAtIndex:j];
   if ([key containsString:@"property"]) {
   properties = [plugin.objects objectForKey:key];

   // Skip invisible polyline
   isVisible = (NSNumber *)[properties objectForKey:@"isVisible"];
   if ([isVisible boolValue] == NO) {
   //NSLog(@"--> key = %@, isVisible = NO", key);
   continue;
   }

   // Skip isClickable polyline
   isClickable = (NSNumber *)[properties objectForKey:@"isClickable"];
   if ([isClickable boolValue] == NO) {
   //NSLog(@"--> key = %@, isClickable = NO", key);
   continue;
   }
   //NSLog(@"--> key = %@, isVisible = YES, isClickable = YES", key);

   // Skip if the click point is out of the polyline bounds.
   bounds = (GMSCoordinateBounds *)[properties objectForKey:@"bounds"];
   if ([bounds containsCoordinate:coordinate]) {
   [boundsHitList addObject:key];
   [boundsPluginList addObject:plugin];
   }
   }
   }
   }
   */


  CLLocationCoordinate2D origin = [self.map.projection coordinateForPoint:CGPointMake(0, 0)];
  CLLocationCoordinate2D hitArea = [self.map.projection coordinateForPoint:CGPointMake(1, 1)];
  CLLocationDistance threshold = GMSGeometryDistance(origin, hitArea);



  //
  maxZIndex = -1;
  for (i = 0; i < [boundsHitList count]; i++) {
    key = [boundsHitList objectAtIndex:i];
    //plugin = [boundsPluginList objectAtIndex:i];
    properties = [self.objects objectForKey:key];


    zIndex = [[properties objectForKey:@"zIndex"] floatValue];
    //NSLog(@"--> zIndex = %f, maxZIndex = %f", zIndex, maxZIndex);
    if (zIndex < maxZIndex) {
      continue;
    }

    if ([key hasPrefix:@"polyline_"]) {
      geodesic = (NSNumber *)[properties objectForKey:@"geodesic"];
      path = (GMSPath *)[properties objectForKey:@"mutablePath"];
      if ([geodesic boolValue] == YES) {
        if ([PluginUtil isPointOnTheGeodesicLine:path coordinate:coordinate threshold:threshold]) {

          maxZIndex = zIndex;
          hitKey = [key stringByReplacingOccurrencesOfString:@"_property" withString:@""];
          continue;
        }
      } else {
        if ([PluginUtil isPointOnTheLine:path coordinate:coordinate projection:self.map.projection]) {
          maxZIndex = zIndex;
          hitKey = [key stringByReplacingOccurrencesOfString:@"_property" withString:@""];
          continue;
        }
      }
    }

    if ([key hasPrefix:@"polygon_"]) {
      path = (GMSPath *)[properties objectForKey:@"mutablePath"];
      if ([PluginUtil isPolygonContains:path coordinate:coordinate projection:self.map.projection]) {
        maxZIndex = zIndex;
        hitKey = [key stringByReplacingOccurrencesOfString:@"_property" withString:@""];
        continue;
      }
    }


    if ([key hasPrefix:@"circle_"]) {
      key = [key stringByReplacingOccurrencesOfString:@"_property" withString:@""];
      GMSCircle *circle = (GMSCircle *)[self.objects objectForKey:key];
      if ([PluginUtil isCircleContains:circle coordinate:coordinate]) {
        maxZIndex = zIndex;
        hitKey = key;
        continue;
      }
    }

    if ([key hasPrefix:@"groundoverlay_"]) {
      key = [key stringByReplacingOccurrencesOfString:@"_property" withString:@""];
      GMSGroundOverlay *groundOverlay = (GMSGroundOverlay *)[self.objects objectForKey:key];
      if ([groundOverlay.bounds containsCoordinate:coordinate]) {
        maxZIndex = zIndex;
        hitKey = key;
        continue;
      }
    }

  }

  if (hitKey != nil) {
    NSArray *tmp = [hitKey componentsSeparatedByString:@"_"];
    NSString *eventName = [NSString stringWithFormat:@"%@_click", [tmp objectAtIndex:0]];
    [self triggerOverlayEvent:eventName overlayId:hitKey coordinate:coordinate];
  } else {
    [self triggerMapEvent:@"map_click" coordinate:coordinate];
  }


}
/**
 * @callback map long_click
 */
- (void) mapView:(GMSMapView *)mapView didLongPressAtCoordinate:(CLLocationCoordinate2D)coordinate {
  [self triggerMapEvent:@"map_long_click" coordinate:coordinate];
}

/**
 * @callback plugin.google.maps.event.CAMERA_MOVE_START
 */
- (void) mapView:(GMSMapView *)mapView willMove:(BOOL)gesture
{
  self.isDragging = gesture;

  if (self.isDragging) {
    [self triggerMapEvent:@"map_drag_start"];
  }
  [self triggerCameraEvent:@"camera_move_start" position:self.map.camera];
}


/**
 * @callback plugin.google.maps.event.CAMERA_MOVE
 */
- (void)mapView:(GMSMapView *)mapView didChangeCameraPosition:(GMSCameraPosition *)position {

  if (self.isDragging) {
    [self triggerMapEvent:@"map_drag"];
  }
  [self triggerCameraEvent:(@"camera_move") position:position];
}

/**
 * @callback plugin.google.maps.event.CAMERA_MOVE_END
 */
- (void) mapView:(GMSMapView *)mapView idleAtCameraPosition:(GMSCameraPosition *)position
{
  if (self.isDragging) {
    [self triggerMapEvent:@"map_drag_end"];
  }
  [self triggerCameraEvent:(@"camera_move_end") position:position];
  self.isDragging = NO;

  // LATITUDE DASHED : Update polylines dashed
  CGFloat scale = 1 / [self.map.projection pointsForMeters:1 atCoordinate:self.map.camera.target];

  for (NSString *key in self.objects) {
    NSObject *obj = [self.objects objectForKey:key];
    
    if ([obj isKindOfClass:[GMSPolyline class]]) {
      GMSPolyline *polyline = (GMSPolyline *) obj;
      
      NSObject *dashed = [polyline userData];
      
      if (dashed) {
        NSArray *styles = @[[GMSStrokeStyle solidColor:polyline.strokeColor],
                            [GMSStrokeStyle solidColor:[UIColor clearColor]]];
        
        NSArray *lengths = @[@([[dashed valueForKey:@"size"] floatValue] * scale), @([[dashed valueForKey:@"offset"] floatValue] * scale)];
        
        if (self.map.camera.zoom >= 8) {
          polyline.spans = GMSStyleSpans(polyline.path, styles, lengths, kGMSLengthRhumb);
        } else {
          polyline.spans = nil;
        }
      }
    }
  }
  // END LATITUDE DASHED
}


/**
 * @callback marker info_click
 */
- (void) mapView:(GMSMapView *)mapView didTapInfoWindowOfMarker:(GMSMarker *)marker
{
  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  if ([markerTag hasPrefix:@"markercluster_"]) {
    [self triggerClusterEvent:@"info_click" marker:marker];
  } else {
    [self triggerMarkerEvent:@"info_click" marker:marker];
  }
  [self syncInfoWndPosition];
}
/**
 * Called after a marker's info window has been long pressed.
 */
- (void)mapView:(GMSMapView *)mapView didLongPressInfoWindowOfMarker:(GMSMarker *)marker {

  [self syncInfoWndPosition];
  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  if ([markerTag hasPrefix:@"markercluster_"]) {
    [self triggerClusterEvent:@"info_long_click" marker:marker];
  } else {
    [self triggerMarkerEvent:@"info_long_click" marker:marker];
  }
}
/**
 * @callback plugin.google.maps.event.MARKER_DRAG_START
 */
- (void) mapView:(GMSMapView *) mapView didBeginDraggingMarker:(GMSMarker *)marker
{
  [self syncInfoWndPosition];
  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  if ([markerTag hasPrefix:@"markercluster_"]) {
    [self triggerClusterEvent:@"marker_drag_start" marker:marker];
  } else {
    [self triggerMarkerEvent:@"marker_drag_start" marker:marker];
  }
}
/**
 * @callback plugin.google.maps.event.MARKER_DRAG_END
 */
- (void) mapView:(GMSMapView *) mapView didEndDraggingMarker:(GMSMarker *)marker
{
  [self syncInfoWndPosition];
  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  if ([markerTag hasPrefix:@"markercluster_"]) {
    [self triggerClusterEvent:@"marker_drag_end" marker:marker];
  } else {
    [self triggerMarkerEvent:@"marker_drag_end" marker:marker];
  }
}
/**
 * @callback plugin.google.maps.event.MARKER_DRAG
 */
- (void) mapView:(GMSMapView *) mapView didDragMarker:(GMSMarker *)marker
{
  [self syncInfoWndPosition];
  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  if ([markerTag hasPrefix:@"markercluster_"]) {
    [self triggerClusterEvent:@"marker_drag" marker:marker];
  } else {
    [self triggerMarkerEvent:@"marker_drag" marker:marker];
  }
}

- (void) syncInfoWndPosition {
  if (self.activeMarker == nil) {
    //NSLog(@"-->no active marker");
    return;
  }
  CLLocationCoordinate2D position = self.activeMarker.position;
  CGPoint point = [self.map.projection
                   pointForCoordinate:CLLocationCoordinate2DMake(position.latitude, position.longitude)];
  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: 'syncPosition', callback: '_onSyncInfoWndPosition', args: [{x: %f, y: %f}]});",
                        self.mapId, point.x, point.y ];
  [self execJS:jsString];
}

/**
 * @callback plugin.google.maps.event.MARKER_CLICK
 */
- (BOOL)mapView:(GMSMapView *)mapView didTapMarker:(GMSMarker *)marker {

  NSString *clusterId_markerId = [NSString stringWithString:marker.userData];

  if ([clusterId_markerId containsString:@"markercluster_"]) {
    if ([clusterId_markerId containsString:@"-marker_"]) {
      //NSLog(@"--->activeMarker = %@", marker.userData);
      self.map.selectedMarker = marker;
      self.activeMarker = marker;
      NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
      if ([markerTag hasPrefix:@"markercluster_"]) {
        [self triggerClusterEvent:@"marker_click" marker:marker];
      } else {
        [self triggerMarkerEvent:@"marker_click" marker:marker];
      }
    } else {
      if (self.activeMarker != nil) {
        NSString *markerTag = [NSString stringWithFormat:@"%@", self.activeMarker.userData];
        if ([markerTag hasPrefix:@"markercluster_"]) {
          if ([markerTag containsString:@"-marker_"]) {
            [self triggerClusterEvent:@"info_close" marker:self.activeMarker];
          }
        } else {
          [self triggerMarkerEvent:@"info_close" marker:self.activeMarker];
        }
      }
      [self triggerClusterEvent:@"cluster_click" marker:marker];
    }
  } else {
    [self execJS:@"javascript:if(window.cordova){cordova.fireDocumentEvent('plugin_touch', {});}"];
    [self triggerMarkerEvent:@"marker_click" marker:marker];
    //NSLog(@"--->activeMarker = %@", marker.userData);
    self.map.selectedMarker = marker;
    self.activeMarker = marker;
  }

  //NSArray *tmp = [clusterId_markerId componentsSeparatedByString:@"_"];
  //NSString *className = [tmp objectAtIndex:0];

  // Get the marker plugin
  //NSString *pluginId = [NSString stringWithFormat:@"%@-%@", self.mapId, className];
  //CDVPlugin<MyPlgunProtocol> *plugin = [self.plugins objectForKey:pluginId];

  // Get the marker properties
  NSString *markerPropertyId = [NSString stringWithFormat:@"marker_property_%@", clusterId_markerId];
  NSDictionary *properties = [self.objects objectForKey:markerPropertyId];

  BOOL disableAutoPan = false;
  if ([properties objectForKey:@"disableAutoPan"] != nil) {
    disableAutoPan = [[properties objectForKey:@"disableAutoPan"] boolValue];
    if (disableAutoPan) {
      return YES;
    }
  }

  //--------------------------
  // Pan the camera mondatory
  //--------------------------
  GMSCameraPosition* cameraPosition = [GMSCameraPosition
                                       cameraWithTarget:marker.position zoom:self.map.camera.zoom];

  [self.map animateToCameraPosition:cameraPosition];
  return YES;
}

- (void)mapView:(GMSMapView *)mapView didCloseInfoWindowOfMarker:(nonnull GMSMarker *)marker {

  // Get the marker plugin
  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  if ([markerTag hasPrefix:@"markercluster_"]) {
    if ([markerTag containsString:@"-marker_"]) {
      [self triggerClusterEvent:@"info_close" marker:marker];
    }
  } else {
    [self triggerMarkerEvent:@"info_close" marker:marker];
  }

  //self.map.selectedMarker = nil; // <-- this causes the didCloseInfoWindowOfMarker event again
  //self.activeMarker = nil; // <-- This causes HTMLinfoWindow is not able to close when you tap on the map.
}


/*
 - (void)mapView:(GMSMapView *)mapView didTapOverlay:(GMSOverlay *)overlay {
 NSString *overlayClass = NSStringFromClass([overlay class]);
 if ([overlayClass isEqualToString:@"GMSPolygon"] ||
 [overlayClass isEqualToString:@"GMSPolyline"] ||
 [overlayClass isEqualToString:@"GMSCircle"] ||
 [overlayClass isEqualToString:@"GMSGroundOverlay"]) {
 [self triggerOverlayEvent:@"overlay_click" id:overlay.title];
 }
 }
 */

/**
 * Map tiles are loaded
 */
- (void) mapViewDidFinishTileRendering:(GMSMapView *)mapView {
  [self triggerMapEvent:@"map_loaded"];
}

/**
 * plugin.google.maps.event.MAP_***()) events
 */
- (void)triggerMapEvent: (NSString *)eventName
{

  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: '%@', callback: '_onMapEvent', args: []});",
                        self.mapId, eventName];
  [self execJS:jsString];
}

/**
 * plugin.google.maps.event.MAP_***(new google.maps.LatLng(lat,lng)) events
 */
- (void)triggerMapEvent: (NSString *)eventName coordinate:(CLLocationCoordinate2D)coordinate
{

  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: '%@', callback: '_onMapEvent', args: [new plugin.google.maps.LatLng(%f,%f)]});",
                        self.mapId, eventName, coordinate.latitude, coordinate.longitude];
  [self execJS:jsString];
}

/**
 * plugin.google.maps.event.CAMERA_*** events
 */
- (void)triggerCameraEvent: (NSString *)eventName position:(GMSCameraPosition *)position
{

  NSMutableDictionary *target = [NSMutableDictionary dictionary];
  [target setObject:[NSNumber numberWithDouble:position.target.latitude] forKey:@"lat"];
  [target setObject:[NSNumber numberWithDouble:position.target.longitude] forKey:@"lng"];

  NSMutableDictionary *json = [NSMutableDictionary dictionary];
  [json setObject:[NSNumber numberWithFloat:position.bearing] forKey:@"bearing"];
  [json setObject:target forKey:@"target"];
  [json setObject:[NSNumber numberWithDouble:position.viewingAngle] forKey:@"tilt"];
  [json setObject:[NSNumber numberWithInt:(int)position.hash] forKey:@"hashCode"];
  [json setObject:[NSNumber numberWithFloat:position.zoom] forKey:@"zoom"];


  GMSVisibleRegion visibleRegion = self.map.projection.visibleRegion;
  GMSCoordinateBounds *bounds = [[GMSCoordinateBounds alloc] initWithRegion:visibleRegion];
  NSMutableDictionary *northeast = [NSMutableDictionary dictionary];
  NSMutableDictionary *southwest = [NSMutableDictionary dictionary];

  [northeast setObject:[NSNumber numberWithFloat:bounds.northEast.latitude] forKey:@"lat"];
  [northeast setObject:[NSNumber numberWithFloat:bounds.northEast.longitude] forKey:@"lng"];
  [json setObject:northeast forKey:@"northeast"];
  [southwest setObject:[NSNumber numberWithFloat:bounds.southWest.latitude] forKey:@"lat"];
  [southwest setObject:[NSNumber numberWithFloat:bounds.southWest.longitude] forKey:@"lng"];
  [json setObject:southwest forKey:@"southwest"];



  NSMutableDictionary *farLeft = [NSMutableDictionary dictionary];
  [farLeft setObject:[NSNumber numberWithFloat:visibleRegion.farLeft.latitude] forKey:@"lat"];
  [farLeft setObject:[NSNumber numberWithFloat:visibleRegion.farLeft.longitude] forKey:@"lng"];
  [json setObject:farLeft forKey:@"farLeft"];

  NSMutableDictionary *farRight = [NSMutableDictionary dictionary];
  [farRight setObject:[NSNumber numberWithFloat:visibleRegion.farRight.latitude] forKey:@"lat"];
  [farRight setObject:[NSNumber numberWithFloat:visibleRegion.farRight.longitude] forKey:@"lng"];
  [json setObject:farRight forKey:@"farRight"];

  NSMutableDictionary *nearLeft = [NSMutableDictionary dictionary];
  [nearLeft setObject:[NSNumber numberWithFloat:visibleRegion.nearLeft.latitude] forKey:@"lat"];
  [nearLeft setObject:[NSNumber numberWithFloat:visibleRegion.nearLeft.longitude] forKey:@"lng"];
  [json setObject:nearLeft forKey:@"nearLeft"];

  NSMutableDictionary *nearRight = [NSMutableDictionary dictionary];
  [nearRight setObject:[NSNumber numberWithFloat:visibleRegion.nearRight.latitude] forKey:@"lat"];
  [nearRight setObject:[NSNumber numberWithFloat:visibleRegion.nearRight.longitude] forKey:@"lng"];
  [json setObject:nearRight forKey:@"nearRight"];

  NSData* jsonData = [NSJSONSerialization dataWithJSONObject:json options:0 error:nil];
  NSString* sourceArrayString = [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: '%@', callback: '_onCameraEvent', args: [%@]});",
                        self.mapId, eventName, sourceArrayString];
  [self execJS:jsString];

  [self syncInfoWndPosition];
}

- (void)execJS: (NSString *)jsString {
  if ([self.webView respondsToSelector:@selector(stringByEvaluatingJavaScriptFromString:)]) {
    [self.webView performSelector:@selector(stringByEvaluatingJavaScriptFromString:) withObject:jsString];
  } else if ([self.webView respondsToSelector:@selector(evaluateJavaScript:completionHandler:)]) {
    [self.webView performSelector:@selector(evaluateJavaScript:completionHandler:) withObject:jsString withObject:nil];
  }
}

/**
 * cluster_*** events
 */
- (void)triggerClusterEvent: (NSString *)eventName marker:(GMSMarker *)marker
{
  if (marker.userData == nil) {
    return;
  }

  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  NSArray *tmp = [markerTag componentsSeparatedByString:@"-"];
  NSString *clusterId = [tmp objectAtIndex:0];
  NSString *markerId = [tmp objectAtIndex:1];

  // Get the marker plugin
  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: '%@', callback: '_onClusterEvent', args: ['%@', '%@', new plugin.google.maps.LatLng(%f, %f)]});",
                        self.mapId, eventName, clusterId, markerId,
                        marker.position.latitude,
                        marker.position.longitude];
  [self execJS:jsString];
}

/**
 * plugin.google.maps.event.MARKER_*** events
 */
- (void)triggerMarkerEvent: (NSString *)eventName marker:(GMSMarker *)marker
{

  NSString *markerTag = [NSString stringWithFormat:@"%@", marker.userData];
  NSArray *tmp = [markerTag componentsSeparatedByString:@"-"];
  NSString *markerId = [tmp objectAtIndex:([tmp count] - 1)];

  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: '%@', callback: '_onMarkerEvent', args: ['%@', new plugin.google.maps.LatLng(%f, %f)]});",
                        self.mapId, eventName, markerId,
                        marker.position.latitude,
                        marker.position.longitude];
  [self execJS:jsString];
}

/**
 * Involve App._onOverlayEvent
 */
- (void)triggerOverlayEvent: (NSString *)eventName overlayId:(NSString*)overlayId coordinate:(CLLocationCoordinate2D)coordinate
{
  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: '%@', callback: '_onOverlayEvent', args: ['%@', new plugin.google.maps.LatLng(%f, %f)]});",
                        self.mapId, eventName, overlayId, coordinate.latitude, coordinate.longitude];
  [self execJS:jsString];
}

-(UIImage *)loadImageFromGoogleMap:(NSString *)fileName {
  NSString *imagePath = [[NSBundle bundleWithIdentifier:@"com.google.GoogleMaps"] pathForResource:fileName ofType:@"png"];
  return [[UIImage alloc] initWithContentsOfFile:imagePath];
}


- (void) didChangeActiveBuilding: (GMSIndoorBuilding *)building {
  if (building == nil) {
    return;
  }
  //Notify to the JS
  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: 'indoor_building_focused', callback: '_onMapEvent'});",
                        self.mapId];
  [self execJS:jsString];
}

- (void) didChangeActiveLevel: (GMSIndoorLevel *)activeLevel {

  if (activeLevel == nil) {
    return;
  }
  GMSIndoorBuilding *building = self.map.indoorDisplay.activeBuilding;

  NSMutableDictionary *result = [NSMutableDictionary dictionary];

  NSUInteger activeLevelIndex = [building.levels indexOfObject:activeLevel];
  [result setObject:[NSNumber numberWithInteger:activeLevelIndex] forKey:@"activeLevelIndex"];
  [result setObject:[NSNumber numberWithInteger:building.defaultLevelIndex] forKey:@"defaultLevelIndex"];

  GMSIndoorLevel *level;
  NSMutableDictionary *levelInfo;
  NSMutableArray *levels = [NSMutableArray array];
  for (level in building.levels) {
    levelInfo = [NSMutableDictionary dictionary];

    [levelInfo setObject:[NSString stringWithString:level.name] forKey:@"name"];
    [levelInfo setObject:[NSString stringWithString:level.shortName] forKey:@"shortName"];
    [levels addObject:levelInfo];
  }
  [result setObject:levels forKey:@"levels"];

  NSError *error;
  NSData *data = [NSJSONSerialization dataWithJSONObject:result options:NSJSONWritingPrettyPrinted error:&error];


  NSString *JSONstring = [[NSString alloc] initWithData:data
                                               encoding:NSUTF8StringEncoding];

  //Notify to the JS
  NSString* jsString = [NSString
                        stringWithFormat:@"javascript:cordova.fireDocumentEvent('%@', {evtName: 'indoor_level_activated', callback: '_onMapEvent', args: [%@]});",
                        self.mapId, JSONstring];

  [self execJS:jsString];
}

@end
